1. MEMBUAT DATABASE

CREATE DATABASE myshop;

2. MEMBUAT TABEL

USERS
CREATE TABLE users(
  id int AUTO_INCREMENT,
  name varchar(255),
  email varchar(255),
  password varchar(255),
  PRIMARY KEY (id)
);

CATEGORIES
CREATE TABLE categories (
  id int AUTO_INCREMENT,
  name varchar(255),
  PRIMARY KEY(id)
  );

ITEMS
CREATE TABLE items (
  id int AUTO_INCREMENT,
  name varchar(255),
  description varchar(255),
  price int,
  stock int,
  category_id int,
  PRIMARY KEY(id),
  FOREIGN KEY (category_id) REFERENCES categories(id)
  );



3. MEMASUKKAN DATA

INSERT INTO users (name,email,password) VALUES
("John Doe","john@doe.com","john123"),
("Jane Doe","jane@doe.com","jenita123");

INSERT INTO categories (name) VALUES
("gadget"),
("cloth"),
("men"),
("women"),
("branded");

INSERT INTO items (name,description,price,stock,category_id) VALUES
("SUMSANG b50","hape keren dari merek samsung",4000000,100,1),
("Uniklooh","baju keren dari brand ternama",500000,50,2),
("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);



4. SQL Query

a. SELECT name,email FROM users;

b. SELECT * FROM items WHERE price>1000000;
   SELECT * FROM items WHERE name LIKE "%watch%";

c. SELECT i.name, i.description, i.price, i.stock, i.category_id, c.name AS kategori FROM items i
   INNER JOIN categories c ON i.category_id = c.id;



5. MENGUBAH DATA

UPDATE items
SET price = 2500000
WHERE id = 1;

